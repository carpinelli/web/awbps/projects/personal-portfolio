import React from "react";


type AppProps =
{
  title: string;
  link: string;
  img: string;
  description: string;
};


const Project = function({ title, link, img, description }: AppProps)
{
  return (
    <section>
      <a href={link}>
        <p>{title}</p>
        <img src={img} alt={img} />
      </a>
      <p>{description}</p>
    </section>
  );
};


export default Project;

