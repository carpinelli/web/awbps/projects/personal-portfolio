import React from "react";


type AppProps =
{
  text: string, tooltipFrom: string
};


const Tooltip = function({ text, tooltipFrom = "tooltip-bottom" }: AppProps)
{
    return (
        <span className={`tooltip ${tooltipFrom}`}>
            {text}
        </span>
    );
};


export default Tooltip;
