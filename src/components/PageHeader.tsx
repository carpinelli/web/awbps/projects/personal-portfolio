import React from "react";


type AppProps = 
{
  text: string;
};


const PageHeader = function({ text }: AppProps)
{
    return (
        <section className="header component">
            <h1>{text}</h1>
            <br />
        </section>
    );
};


export default PageHeader;
