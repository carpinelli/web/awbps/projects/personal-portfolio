import React from "react";
import { useRef } from "react";

import NavItem from "./NavItem";


const MobileNav = function()
{
  // const mobileMenuRef = useRef<HTMLElement | null>(null);
  const mobileMenuRef = useRef<any>(null);
  const tooltipFrom = "tooltip-left";

  const handleClick = function(_: React.MouseEvent<HTMLDivElement, MouseEvent>)
  {
    console.log(_);
    if (mobileMenuRef.current)
    {
      mobileMenuRef.current.classList.toggle("hidden");
    }
  };

  
  return (
    <div onClick={handleClick} className="nav-mobile">
      <div className="hidden mobile-menu" ref={mobileMenuRef}>
        <NavItem linkpath="/mern" text="MERN Stack Projects" tooltipFrom={tooltipFrom} />
        <NavItem linkpath="/frontend" text="Frontend Projects" tooltipFrom={tooltipFrom} />
        {/* <NavItem linkpath="/backend" text="Backend Projects" tooltipFrom={tooltipFrom} /> */}
        <NavItem linkpath="/about" text="About" tooltipFrom={tooltipFrom} />
      </div>
      {/* Get Hamburger Menu Icon */}
      <svg xmlns="https://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
        stroke="currentColor" className="hamburger-icon">
        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
          d="M4 6h16M4 12h16M4 18h16" />
      </svg>
    </div>
  );
};


export default MobileNav;
