import React from "react";
import { Link } from "react-router-dom";

import Tooltip from "../Tooltip";


type AppProps = 
{
  linkpath: string;
  text: string;
  tooltip?: string;
  tooltipFrom?: string;
}


const NavItem = function(
    {
        linkpath,
        text,
        tooltip = text,
        tooltipFrom = "tooltip-bottom",
    }: AppProps)
{
    // Home link is h2, the rest are h4.
    const headerJSX = (linkpath === "/"
                       ? <h2 className="nav-component">{text}</h2>
                       : <h4 className="nav-component">{text}</h4>);

    return (
        <Link to={linkpath}>
            <div className="group">
                {headerJSX}
                <Tooltip text={tooltip} tooltipFrom={tooltipFrom} />
            </div>
        </Link>
    );
};


export default NavItem;
