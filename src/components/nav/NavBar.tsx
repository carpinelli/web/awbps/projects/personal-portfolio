import React from "react";

import NavItem from "./NavItem";
import FullNav from "./FullNav";
import MobileNav from "./MobileNav";


const NavBar = function()
{
    return (
        <section className="nav-bar component">
            <div className="home-logo nav-component">
                <NavItem
                 linkpath="/" text="Joseph's Portfolio" tooltip="Home Page" />
            </div>
            <MobileNav />
            <FullNav />
        </section>
    );
};


export default NavBar;
