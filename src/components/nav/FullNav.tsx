import React from "react";
import NavItem from "./NavItem";


const FullNav = function()
{
    return (
        <div className="nav-full">
            <div className="site-sections">
                <NavItem linkpath="/mern" text="MERN Stack Projects" />
                <NavItem linkpath="/frontend" text="Frontend Projects" />
                {/* <NavItem linkpath="/backend" text="Backend Projects" /> */}
            </div>
            <div className="about-section">
                <NavItem linkpath="/about" text="About" />
            </div>
        </div>
    );
};


export default FullNav;
