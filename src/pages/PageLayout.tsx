import React from "react";
import NavBar from "../components/nav/NavBar";


type AppProps = {children: any};

const PageLayout = function({ children }: AppProps)
{
    return (
        <article>
            <NavBar />
            {children}
        </article>
    );
};


export default PageLayout;
