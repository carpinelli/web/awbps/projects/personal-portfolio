import React from "react";

import PageHeader from "../components/PageHeader";
import Project from "../components/Project";
import shapeRedImg from "../assets/images/shape-red-screenshot-1.png";
import pokeSavPlannerImg from "../assets/images/pokesav-planner-screenshot-3.png";


const Frontend = function()
{
  // Shape Red.
  const shapeRedLink: string = "https://gitlab.com/carpinelli/web/awbps/projects/shape-red/";
  // const shapeRedImg: string = "../../public/assets/images/shape-red-screenshot-1.png";
  const shapeRedDescription: string = `
    A shape versus DOM game meant to look similar in style to the first game in a popular franchise, but much simplified to meet project requirements.

    Battle as, and against, one of three types of shapes, with each having a unique strength and weakness.
  `;
  const shapeRedProject = (
    <Project title="Shape Red" link={shapeRedLink}
      img={shapeRedImg} description={shapeRedDescription} />
  );
  // PokeSav Planner.
  const pokeSavPlannerLink: string = "https://gitlab.com/carpinelli/web/awbps/projects/pokesav-planner/";
  // const pokeSavPlannerImg: string = "../../public/assets/images/pokesav-planner-screenshot-3.png";
  const pokeSavPlannerDescription: string = `
    A React/Redux application that uses the https://www.pokeapi.co/ API to 
    help plan main line game playthroughs.
  `;
  const pokeSavPlannerProject = (
    <Project title="PokeSav Planner" link={pokeSavPlannerLink}
      img={pokeSavPlannerImg} description={pokeSavPlannerDescription} />
  );

  return (
    <section className="page">
      <PageHeader text="Frontend Projects" />
      <section>
        {shapeRedProject}
        {pokeSavPlannerProject}
      </section>
    </section>
  );
};


export default Frontend;

