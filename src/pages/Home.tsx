import React from "react";

import PageHeader from "../components/PageHeader";
import Project from "../components/Project";
import shapeRedImg from "../assets/images/shape-red-screenshot-1.png";
import pokeSavPlannerImg from "../assets/images/pokesav-planner-screenshot-3.png";


const Home = function()
{
  // Shape Red.
  const shapeRedLink: string = "https://gitlab.com/carpinelli/web/awbps/projects/shape-red/";
  // const shapeRedImg: string = "../../public/assets/images/shape-red-screenshot-1.png";
  const shapeRedDescription: string = `
    A shape versus DOM game meant to look similar in style to the first game in a popular franchise, but much simplified to meet project requirements.

    Battle as, and against, one of three types of shapes, with each having a unique strength and weakness.
  `;
  const shapeRedProject = (
    <Project title="Shape Red" link={shapeRedLink}
      img={shapeRedImg} description={shapeRedDescription} />
  );
  // PokeSav Planner.
  const pokeSavPlannerLink: string = "https://gitlab.com/carpinelli/web/awbps/projects/pokesav-planner/";
  // const pokeSavPlannerImg: string = "../../public/assets/images/pokesav-planner-screenshot-3.png";
  const pokeSavPlannerDescription: string = `
    A React/Redux application that uses the https://www.pokeapi.co/ API to 
    help plan main line game playthroughs.
  `;
  const pokeSavPlannerProject = (
    <Project title="PokeSav Planner" link={pokeSavPlannerLink}
      img={pokeSavPlannerImg} description={pokeSavPlannerDescription} />
  );
  // Room Planner.
  const roomPlannerLink: string = "https://gitlab.com/carpinelli/web/awbps/projects/room-planner/";
  const roomPlannerImg: string = "/IMAGE/PATH";
  const roomPlannerDescription: string = `
    A MERN stack application for planning the rearrangement of rooms before
    having to move anything physically. Some intended features are to store
    and save rooms, furniture, and previously created layouts.
  `;
  const roomPlannerProject = (
    <Project title="Room Planner" link={roomPlannerLink}
      img={roomPlannerImg} description={roomPlannerDescription} />
  );

  return (
    <section className="page">
      <PageHeader text="Home Page" />
      <section>
        <h1>Welcome to Joseph's Portfolio!</h1>
        <hr /><br />
        <h2>About Me</h2>
        <p>I’m a lifelong learner who is fascinated with computer science because I believe it is the best tool we have for unlocking the limits and nature of this universe. I truly enjoy focusing on improving myself and my environment. I have a great passion for Linux and open source. Currently, I’m looking to expand my skills and am working on several additional frameworks and languages. I am looking to secure a long-term position as a Developer in a challenging environment that encourages and facilitates learning, growth, and a cohesive team.</p>
        <hr /><br />
        <h2>Skills</h2>
        <p>• MERN Web Stack + TailwindCSS</p>
        <p>• Python</p>
        <p>• Excel VBA</p>
        <p>• bash</p>
        <p>• git</p>
        <p>• vim</p>
        <br /><br />
      </section>
      <section>
        <h2>MERN Stack Projects</h2>
        {roomPlannerProject}
        <hr /><br />
        <h2>Frontend Projects</h2>
        {shapeRedProject}
        {/* <a href={pokeSavPlannerLink}><p>• PokeSave Planner</p></a> */}
        {pokeSavPlannerProject}
        <br />
      </section>
      <section>
        {/* <h2>Backend Projects</h2> */}
        <br />
      </section>
    </section>
  );
};



export default Home;
