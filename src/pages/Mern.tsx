import React from "react";

import PageHeader from "../components/PageHeader";
import Project from "../components/Project";


const Mern = function()
{
  // Room Planner.
  const roomPlannerLink: string = "https://gitlab.com/carpinelli/web/awbps/projects/room-planner/";
  const roomPlannerImg: string = "/IMAGE/PATH";
  const roomPlannerDescription: string = `
    A MERN stack application for planning the rearrangement of rooms before
    having to move anything physically. Some intended features are to store
    and save rooms, furniture, and previously created layouts.
  `;
  const roomPlannerProject = (
    <Project title="Room Planner" link={roomPlannerLink}
      img={roomPlannerImg} description={roomPlannerDescription} />
  );

  return (
    <section className="page">
      <PageHeader text="MERN Stack Projects" />
      <section>
        {roomPlannerProject}
      </section>
    </section>
  );
};


export default Mern;

