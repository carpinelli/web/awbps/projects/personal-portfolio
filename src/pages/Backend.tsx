import React from "react";

import PageHeader from "../components/PageHeader";
import Project from "../components/Project";


const Backend = function()
{
  const projectNameLink: string = "https://gitlab.com/carpinelli/web/awbps/lorem-ipsum/";
  const projectNameImg: string = "/IMAGE/PATH";
  const projectNameDescription = `
    DESCRIPTION.
  `;


  return (
    <section className="page">
      <PageHeader text="Backend Projects" />
      <section>
        <Project title="Lorem Ipsum" link={projectNameLink}
          img={projectNameImg} description={projectNameDescription} />
      </section>
    </section>
  );
};


export default Backend;

