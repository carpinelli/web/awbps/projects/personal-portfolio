import React from "react";
import ReactMarkdown from "react-markdown";

import README from "../data/README";
import PageHeader from "../components/PageHeader";


const About = function()
{
    return (
        <section className="page">
            <PageHeader text="About Page" />
            <ReactMarkdown className="readme">{README}</ReactMarkdown>
        </section>
    );
};



export default About;
