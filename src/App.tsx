import * as React from "react";
import type { ReactNode } from "react";
import { Route, Routes } from "react-router-dom";

import PageLayout from "./pages/PageLayout";
import Home from "./pages/Home";
import About from "./pages/About";
import Mern from "./pages/Mern";
import Frontend from "./pages/Frontend";
import Backend from "./pages/Backend";


const App = function()
{
  const home = (<PageLayout><Home /></PageLayout>);
  const about = (<PageLayout><About /></PageLayout>);
  const mernProjects = (<PageLayout><Mern /></PageLayout>);
  const frontendProjects = (<PageLayout><Frontend /></PageLayout>);
  const backendProjects = (<PageLayout><Backend /></PageLayout>);
  const appJsx: ReactNode = (
    <main className="app">
      <Routes>
        <Route path="/" element={home} />
        <Route path="/about" element={about} />
        <Route path="/mern" element={mernProjects} />
        <Route path="/frontend" element={frontendProjects} />
        <Route path="/backend" element={backendProjects} />
      </Routes>
    </main>
  );

  return (
    <>
      {appJsx}
    </>
  );
};

export default App;

