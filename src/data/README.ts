const README = `
# Personal Portfolio Project

A personal portfolio website to showcase web projects.

---


## Deployment Link
[Joseph's Portfolio](https://carpinelli.gitlab.io/web/awbps/projects/personal-portfolio/)

---


## Technologies
* TypeScript
* React
* Redux
* React-Router-DOM
* React-Markdown
* TailwindCSS

---


<!--
## Approach
Lorem ipsum.

## Project Requirements
* [x] B
* [ ] H

### Bonus Requirements
* [x] B
* [ ] H

### Previous React Lessons Not Required
* [x] B
* [ ] H

---


-->

---


## Intended Features
* Project Component


## Ideas
* Bottom or left side NavBar with Buttons instead of Links

---


## Unresolved Issues
* Deployment link is not currently working, no content is added to
  div#root.
* UI switches to mobile to late. The full nav is cutoff.
* Project variables are copied and pasted and should be put into a
  store.

---


## Building
### React/Vite
	cd <PROJECTS/BUILDS_DIRECTORY>
	git clone <GIT_URL> <DIR_NAME>/
	cd <DIR_NAME>/
	npm install
	npm run build

---


## Contact
Joseph Carpinelli <carpinelli.dev@protonmail.ch>.

---


## Contributing
This is mostly a personal project, but any contributions are welcome and
appreciated, provided they align with the licenses, styles, and goals of
the project. Please see the [CONTRIBUTING.md](CONTRIBUTING.md) file, if
available. Forking or copying is encouraged, just ensure to uphold the
licenses terms.

---


## License
The software and related files in this project are licensed under the
AGPL 3.0 license or higher. All non-software related files are licensed
under the Creative Commons Attribution Share Alike 4.0 International
license. Unless any libraries used legally require otherwise, all
contributions are made under the
[GNU Affero General Public License v3 or higher](https://www.gnu.org/licenses/agpl-3.0.html).
See the [LICENSE](LICENSE).

In cases where a library is used that requires a different or additional
license, and that license is not included, kindly inform the email
provided in the "Contact" section. Corrections will be made ASAP.

You can be released from the requirements of the above license by
purchasing a commercial license. Buying such a license is mandatory
if you want to modify or otherwise use the software for commercial
activities involving the software without disclosing the source code
of your own applications. Please note this may not always be possible
due to  the licensing requirements of included projects.

To purchase a commercial license, where available, send an
email to <carpinelli.dev@protonmail.ch>.

---

`;


export default README;

