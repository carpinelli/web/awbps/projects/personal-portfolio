import React from "react";
import type { ReactNode } from "react";
import ReactDOM from "react-dom/client";
import { HashRouter } from "react-router-dom";
// import { BrowserRouter } from "react-router-dom";
// import { Provider } from "react-redux";

import App from "./App";
import "./index.css";
// import { store } from "./state/store";


const root = document.getElementById("root");
const rootJsx: ReactNode = (
  <React.StrictMode>
    {/* <BrowserRouter basename="/web/awbps/projects/pokesav-planner/"> */}
    <HashRouter>
      {/* <Provider store={store}> */}
        <App />
      {/* </Provider> */}
    </HashRouter>
  </React.StrictMode> as ReactNode
);

if (root && rootJsx)
{
  const rootElement = ReactDOM.createRoot(root);
  rootElement.render(rootJsx,);
}

